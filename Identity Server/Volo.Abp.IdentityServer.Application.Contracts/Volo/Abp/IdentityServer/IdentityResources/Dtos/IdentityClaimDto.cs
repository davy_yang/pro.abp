﻿using System;

namespace Volo.Abp.IdentityServer.IdentityResources.Dtos
{
	public class IdentityClaimDto
	{
		public Guid IdentityResourceId { get; set; }

		public string Type { get; set; }
	}
}
