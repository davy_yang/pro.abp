﻿namespace Volo.Abp.Account.Public.Web
{
    public class AbpAccountOptions
	{
		public string WindowsAuthenticationSchemeName { get; set; }

		public AbpAccountOptions()
		{
			this.WindowsAuthenticationSchemeName = "Windows";
		}
	}
}
