﻿using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Volo.Abp.LanguageManagement.EntityFrameworkCore
{
	public class LanguageManagementModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
	{
		public LanguageManagementModelBuilderConfigurationOptions(string tablePrefix = "", string schema = null)
			: base(tablePrefix, schema)
		{
		}
	}
}
