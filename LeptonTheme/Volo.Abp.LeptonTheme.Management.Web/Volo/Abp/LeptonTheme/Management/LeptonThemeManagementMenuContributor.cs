﻿using System.Threading.Tasks;
using Volo.Abp.UI.Navigation;

namespace Volo.Abp.LeptonTheme.Management
{
    public class LeptonThemeManagementMenuContributor : IMenuContributor
	{
		public async Task ConfigureMenuAsync(MenuConfigurationContext context)
		{
			if (context.Menu.Name == "Main")
			{
				await Task.CompletedTask;
			}
		}
	}
}
